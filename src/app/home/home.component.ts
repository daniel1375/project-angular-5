 import { Component, OnInit } from '@angular/core';

import { User } from '../_models/index';
import { UserService } from '../_services/index';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';

@Component({
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

	currentUser = this.firebase.auth.currentUser;

  constructor(private userService: UserService, private firebase: AngularFireAuth) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    console.log(this.currentUser);
  }

  ngOnInit() {
  	// this.loadAllUsers();
  }

  deleteUser(id: number){
  	// this.userService.delete(id).subscribe(() => { this.loadAllUsers() });
  }

  // private loadAllUsers(){
  //   this.userService.getAll().subscribe(users => { this.users = users; });
  //   console.log(this.currentUser);
  // } 

}
