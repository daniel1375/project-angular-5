﻿import { Component } from '@angular/core';

import './app.css';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
})

export class AppComponent { }