import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import { AuthenticationService, AlertService } from '../_services/index';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.css' ]
})
export class LoginComponent implements OnInit {

	model : any = {};
	loading = false;
	returnUrl: string;


    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService) { }

	ngOnInit() {
		this.authenticationService.logout();
		this.returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';
	}

	login(){
		this.loading = true;
		this.authenticationService.login(this.model.mail , this.model.password)
			.then(
				data => {
					this.router.navigate([this.returnUrl]);
				},
				error => {

          let errorCode = error.code;
          let errorMessage = error.message;

          if(errorCode === 'auth/invalid-email') {

            this.alertService.error('Invalid e-mail format');

          }else if(errorCode === 'auth/user-disabled'){

            this.alertService.error('User disabled');

          }else if(errorCode === 'auth/user-not-found'){

            this.alertService.error('User not found');

          }else if(errorCode === 'auth/wrong-password'){

            this.alertService.error('Wrong password');

          }else{

            this.alertService.error(errorMessage);
            
          }

          this.loading = false;
			});
	}

}
