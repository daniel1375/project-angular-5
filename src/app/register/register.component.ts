import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

import { UserService, AlertService } from '../_services/index';
import { User } from '../_models/index';

@Component({
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {
  loading = false;
  user: any = {};

  constructor( private userService: UserService,
  			   private alertService: AlertService,
  			   private router: Router,
           private af: AngularFireDatabase ) { }

  register(){
  	this.loading = true;
  	return this.userService.create(this.user)
  		.then(
  			data => {
  				this.alertService.success('Registration successful', true);
  				this.router.navigate(['/login']);
  			},
  			error => {
  				this.loading = false;
          let errorCode = error.code;
          let errorMessage = error.message;

          if(errorCode === 'auth/email-already-in-use') {

            this.alertService.error('E-mail already in use!');

          }else if(errorCode === 'auth/invalid-email'){

            this.alertService.error('Invalid e-mail!');

          }else if(errorCode === 'auth/operation-not-allowed'){

            this.alertService.error('Operation not allowed!');

          }else if(errorCode === 'auth/weak-password'){

            this.alertService.error('Too weak password!');

          }else{

            this.alertService.error(errorMessage);

          }

  			});
  }

  ngOnInit() {

  }

}
