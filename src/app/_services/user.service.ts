import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';

import { User } from '../_models/index';

@Injectable()
export class UserService{
	constructor(private http: HttpClient, private firebase: AngularFireAuth){}

	getAll(){
		return this.http.get<User[]>('/api/users');
	}

	getById(id: number){
		return this.http.get(`/api/users/${id}`);
	}

	create(user: User){
		return this.firebase.auth.createUserAndRetrieveDataWithEmailAndPassword(user.mail, user.password).then(data => {

			this.firebase.auth.currentUser.updateProfile({
				displayName: user.firstName + " " + user.lastName,
				photoURL: "",
			  }).then(function() {
				// Update successful.
			  }).catch(function(error) {
				// An error happened.
			  });
		});
	}

	update(user: User){
		return this.http.post('/api/users/' + user.id,user);
	}

	delete(id: number){
		return this.http.delete('/api/users/'+ id);
	}

}