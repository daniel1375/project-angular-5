import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import 'rxjs/add/operator/map';
 
@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient, private firebase: AngularFireAuth) { }
 
    login(mail: string, password: string) {

        return this.firebase.auth.signInAndRetrieveDataWithEmailAndPassword(mail,password).then(succes =>{
          this.firebase.auth.currentUser.getIdToken().then( data => { 

            if(data){
              localStorage.setItem('currentUser', JSON.stringify( this.firebase.auth.currentUser ));
            }
            console.log(localStorage.getItem('currentUser'));
            return this.firebase.auth.currentUser;

          });
        });
    }
 
    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}