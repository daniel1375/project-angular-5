// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAklkBO6LkdvNyj9-Gki1zhU1zXwSSDFKQ",
    authDomain: "first-angular-5-project.firebaseapp.com",
    databaseURL: "https://first-angular-5-project.firebaseio.com",
    projectId: "first-angular-5-project",
    storageBucket: "first-angular-5-project.appspot.com",
    messagingSenderId: "678874982094"    
  }
};
